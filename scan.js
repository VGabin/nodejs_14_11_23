const scanf = require('scanf');

const liste = [];
const nbNumber = 2;

for (let index = 0; index < nbNumber; index++) {
    console.log('Entrez un chiffre');
    liste.push(scanf('%d'));
}

liste.forEach(number => {
    if (number % 2 == 0) {
        console.log(number + " est paire");
    }
    else {
        console.log(number + " est impaire");
    }
});