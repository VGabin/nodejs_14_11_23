const scanf = require('scanf');

const nbNumber = 5;
let note = 0;

for (let index = 0; index < nbNumber; index++) {
    console.log('Entrez une note');
    const entry = scanf('%d');
    note += entry;
}

console.log("La note finale est " + note/nbNumber);