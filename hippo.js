class Hippo{
    constructor (name, weight, tusksSize) {
        this.name      = name;
        this.weight    = weight;
        this.tusksSize = tusksSize;
    }
    
    swim() {
        this.weight -= 0.3;
        return actualHippo;
    }

    eat() {
        actualHippo.weight += 1;
        return actualHippo;
    }

    fight(Hippopotamus) {
        switch (actualHippo.tusksSize > Hippopotamus.tusksSize) {
            case true:
                return(actualHippo.name + " remporte la victoire");
            case false:
                return(Hippopotamus.name + " remporte la victoire");
        }
    }
}

const actualHippo = new Hippo("Biboo", 200, 15);
const evilHippo = new Hippo("Bae", 100, 17);


console.log(actualHippo.fight(evilHippo));