# Projet API avec Swagger et Jest

Ce projet est conçu pour fonctionner avec Docker et Node.js. Suivez les étapes ci-dessous pour configurer et exécuter le projet.

## 1ère étape : Configuration du fichier .env

Créez un fichier `.env` en utilisant le modèle fourni dans le fichier `.env.sample`. Remplissez la valeur pour "JWT_TOKEN" selon vos préférences.

## 2ème étape : Exécution avec Docker Compose

Utilisez la commande suivante pour démarrer l'application avec Docker Compose :

```bash
docker-compose up -d
```

L'application sera maintenant accessible à l'adresse [localhost:3000](http://localhost:3000).

## Documentation

Pour accéder à la documentation, rendez-vous sur [localhost:3000/docs](http://localhost:3000/docs).

## Tests unitaires

Pour effectuer les tests unitaires, suivez les étapes ci-dessous :

1. Accédez au conteneur Docker avec la commande suivante :

```bash
docker exec -ti projet-m1-2023 bash
```

2. À l'intérieur du conteneur, exécutez les tests avec la commande npm :

```bash
npm test
```

Cela exécutera les tests unitaires pour le projet.

N'oubliez pas de fermer le conteneur Docker après avoir terminé les tests.