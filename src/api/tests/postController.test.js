const PostController = require('../controllers/postController');
const Post = require('../models/postModel');
const textApiProvider = require('../providers/textApiProvider');

beforeEach(() => {
    jest.mock('../models/postModel');
    jest.mock('../providers/textApiProvider', () => ({
        getRandomText: jest.fn(() => Promise.resolve('Texte aléatoire simulé')),
    }));
});

describe('listAllPosts', () => {
    test('devrait renvoyer une liste de publications avec le statut 200 et non nulle', async () => {
        jest.spyOn(Post, 'find').mockResolvedValue([
            {
                title: 'titre1',
                content: 'contenu1',
            },
            {
                title: 'titre2',
                content: 'contenu2',
            },
        ]);
        
        const req = {};
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        await PostController.listAllPosts(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).not.toBeNull();
    });
});

describe('listAllPosts échec', () => {
    test('devrait renvoyer une liste de publications avec le statut 500 et non nulle', async () => {
        jest.spyOn(Post, 'find').mockResolvedValue();
        
        const req = {};
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        await PostController.listAllPosts(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).not.toBeNull();
    });
});

describe('createAPost', () => {
    test('devrait renvoyer une publication avec le statut 201 et non nulle', async () => {
        jest.spyOn(Post.prototype, 'save').mockResolvedValue();
        
        const req = {
            body: {
                title: 'titre',
                content: 'contenu'
            }
        };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        await PostController.createAPost(req, res);

        expect(res.status).toHaveBeenCalledWith(201);
        expect(res.json).not.toBeNull();
    });
});

describe('createAPost corps vide', () => {
    test('devrait renvoyer une publication avec le statut 400 et non nulle', async () => {
        jest.spyOn(Post.prototype, 'save').mockResolvedValue();
        
        const req = {
            body: {
            }
        };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        await PostController.createAPost(req, res);

        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).not.toBeNull();
    });
});

describe('getAPost', () => {
    test('devrait renvoyer une publication avec le statut 200 et non nulle', async () => {
        jest.spyOn(Post, 'findById').mockResolvedValue({
            title: 'nouveauTitre',
            content: 'nouveauContenu',
            _id: '123456'
        });
        
        const req = {
            params: {
                postId: '123456'
            }
        };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        await PostController.getAPost(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).not.toBeNull();
    });
});

describe('getAPost échec', () => {
    test('devrait renvoyer une publication avec le statut 404 et non nulle', async () => {
        jest.spyOn(Post, 'findById').mockResolvedValue();
        
        const req = {
            params: {
                postId: '123456'
            }
        };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        await PostController.getAPost(req, res);

        expect(res.status).toHaveBeenCalledWith(404);
        expect(res.json).not.toBeNull();
    });
});

describe('editAPost', () => {
    test('devrait renvoyer une publication avec le statut 200 et non nulle', async () => {
        const req = {
            params: {
                postId: '123456'
            },
            body: {
                title: 'nouveauTitre',
                content: 'nouveauContenu'
            }
        };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        // Supposons que l'opération de modification réussisse
        jest.spyOn(Post, 'findByIdAndUpdate').mockResolvedValue({
            title: 'nouveauTitre',
            content: 'nouveauContenu',
            _id: '123456'
        });

        await PostController.editAPost(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).not.toBeNull();
    });
});

describe('deleteAPost', () => {
    test('devrait renvoyer une publication avec le statut 200 et non nulle', async () => {
        const req = {
            params: {
                postId: '123456'
            }
        };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        // Supposons que l'opération de suppression réussisse
        jest.spyOn(Post, 'deleteOne').mockResolvedValue({
            _id: '123456'
        });

        await PostController.deleteAPost(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).not.toBeNull();
    });
});