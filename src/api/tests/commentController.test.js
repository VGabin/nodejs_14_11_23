const CommentController = require('../controllers/commentController');
const Comment = require('../models/commentModel');
const Post = require('../models/postModel');

beforeEach(() => {
    jest.mock('../models/commentModel');
});

describe('listAllComments', () => {
    test('devrait renvoyer une liste de commentaires avec le statut 200 et non nulle si le post existe', async () => {
        jest.spyOn(Post, 'findById').mockResolvedValue({
            _id: '123456',
            title: 'postTitle',
            content: 'postContent'
        });

        jest.spyOn(Comment, 'find').mockResolvedValue([
            {
                name: 'user1',
                message: 'comment1',
            },
            {
                name: 'user2',
                message: 'comment2',
            },
        ]);

        const req = {
            params: {
                postId: '123456'
            }
        };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        await CommentController.listAllComments(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).not.toBeNull();
    });

    test('devrait renvoyer une réponse avec le statut 404 si le post n\'existe pas', async () => {
        jest.spyOn(Post, 'findById').mockResolvedValue(null);

        const req = {
            params: {
                postId: '123456'
            }
        };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        await CommentController.listAllComments(req, res);

        expect(res.status).toHaveBeenCalledWith(404);
        expect(res.json).not.toBeNull();
    });
});

describe('createAComment', () => {
    test('devrait renvoyer un commentaire avec le statut 201 et non nul', async () => {
        jest.spyOn(Comment.prototype, 'save').mockResolvedValue();

        const req = {
            params: {
                postId: '123456'
            },
            body: {
                name: 'userName',
                message: 'userMessage'
            }
        };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        await CommentController.createAComment(req, res);

        expect(res.status).toHaveBeenCalledWith(201);
        expect(res.json).not.toBeNull();
    });
});

describe('createAComment corps vide', () => {
    test('devrait renvoyer un commentaire avec le statut 400 et non nul', async () => {
        jest.spyOn(Comment.prototype, 'save').mockResolvedValue();

        const req = {
            params: {
                postId: '123456'
            },
            body: {}
        };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        await CommentController.createAComment(req, res);

        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).not.toBeNull();
    });
});

describe('getAComment', () => {
    test('devrait renvoyer un commentaire avec le statut 200 et non nul', async () => {
        jest.spyOn(Comment, 'findById').mockResolvedValue({
            name: 'userName',
            message: 'userMessage',
            _id: 'commentId'
        });

        const req = {
            params: {
                commentId: 'commentId'
            }
        };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        await CommentController.getAComment(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).not.toBeNull();
    });
});

describe('getAComment échec', () => {
    test('devrait renvoyer un commentaire avec le statut 404 et non nul', async () => {
        jest.spyOn(Comment, 'findById').mockResolvedValue();

        const req = {
            params: {
                commentId: 'commentId'
            }
        };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        await CommentController.getAComment(req, res);

        expect(res.status).toHaveBeenCalledWith(404);
        expect(res.json).not.toBeNull();
    });
});

describe('editAComment', () => {
    test('devrait renvoyer un commentaire avec le statut 200 et non nul', async () => {
        const req = {
            params: {
                commentId: 'commentId'
            },
            body: {
                name: 'newUserName',
                message: 'newUserMessage'
            }
        };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        // Supposons que l'opération de modification réussisse
        jest.spyOn(Comment, 'findByIdAndUpdate').mockResolvedValue({
            name: 'newUserName',
            message: 'newUserMessage',
            _id: 'commentId'
        });

        await CommentController.editAComment(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).not.toBeNull();
    });
});

describe('deleteAComment', () => {
    test('devrait renvoyer un commentaire avec le statut 200 et non nul', async () => {
        const req = {
            params: {
                commentId: 'commentId'
            }
        };
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };

        // Supposons que l'opération de suppression réussisse
        jest.spyOn(Comment, 'deleteOne').mockResolvedValue({
            _id: 'commentId'
        });

        await CommentController.deleteAComment(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).not.toBeNull();
    });
});