const Post = require ("../models/postModel");
const textApiProdiver = require("../providers/textApiProvider");

exports.listAllPosts = async (req, res) => {
    try {
        const posts = await Post.find({});
        res.status(200);
        res.json(posts);
    } catch (error) {
        res.status(500);
        console.log(error);
        res.json({ message : "erreur serveur"})
    }
}

exports.createAPost = async (req, res) => {
    const newPost = new Post(req.body);

    if (!newPost.title || !newPost.content) {
        return res.status(400).json({ message: "Le titre et le contenu sont obligatoires." });
    }

    try {
        let randomTextPromise = textApiProdiver.getRandomText();
        let response = await randomTextPromise;
        
        if(!newPost.content) {
            newPost.content = response;
        }

        const post = await newPost.save();
        
        res.status(201);
        res.json(post);
    } catch(error) {
        console.log(error);
        res.status(401).json({ message: "Erreur serveur" });
    }

    // try {
    //     const post = await newPost.save();
    //     res.status(201);
    //     res.json(post);
    // } catch (error) {
    //     res.status(500);
    //     console.log(error);
    //     res.json({ message: "Erreur serveur" });
    // }
}


exports.getAPost = async (req, res) => {
    const postId = req.params.postId;
    try {
        const post = await Post.findById(postId);
        if (post) {
            res.status(200);
            res.json(post);
        } else {
            res.status(404);
            res.json({ message: "Post non trouvé" });
        }
    } catch (error) {
        res.status(500);
        console.log(error);
        res.json({ message: "Erreur serveur" });
    }
}

exports.editAPost = async (req, res) => {
    const postId = req.params.postId;
    const updatedPostData = req.body;
    try {
        const updatedPost = await Post.findByIdAndUpdate(postId, updatedPostData, { new: true });
        if (updatedPost) {
            res.status(200);
            res.json(updatedPost);
        } else {
            res.status(404);
            res.json({ message: "Post non trouvé" });
        }
    } catch (error) {
        res.status(500);
        console.log(error);
        res.json({ message: "Erreur serveur" });
    }
}

exports.deleteAPost = async (req, res) => {
    const postId = req.params.postId;
    try {
        const deletedPost = await Post.deleteOne({"_id": postId});
        if (deletedPost) {
            res.status(200);
            res.json({ message: "Post supprimé avec succès" });
        } else {
            res.status(404);
            res.json({ message: "Post non trouvé" });
        }
    } catch (error) {
        res.status(500);
        console.log(error);
        res.json({ message: "Erreur serveur" });
    }
}