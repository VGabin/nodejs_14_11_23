const Comment = require ('../models/commentModel');
const Post = require('../models/postModel');

exports.listAllComments = async (req, res) => {
    try {
        const postId = req.params.postId;

        // Vérifier si le post existe
        const post = await Post.findById(postId);
        if (!post) {
            return res.status(404).json({ message: "Le post n'a pas été trouvé." });
        }

        const comments = await Comment.find({ post: postId });
        res.status(200);
        res.json(comments);
    } catch (error) {
        res.status(500);
        console.log(error);
        res.json({ message: "Erreur serveur" });
    }
}

exports.createAComment = async (req, res) => {
    const { name, message } = req.body;
    const postId = req.params.postId;

    // Vérifier si les paramètres requis sont présents
    if (!name || !message || !postId) {
        return res.status(400).json({ message: "Les paramètres 'name', 'message' et 'postId' sont obligatoires." });
    }

    const newComment = new Comment({
        name,
        message,
        post: postId
    });

    try {
        const comment = await newComment.save();
        res.status(201);
        res.json(comment);
    } catch (error) {
        res.status(500);
        console.log(error);
        res.json({ message: 'Erreur serveur' })
    }
}


exports.getAComment = async (req, res) => {
    const commentId = req.params.commentId;
    try {
        const comment = await Comment.findById(commentId);
        if (comment) {
            res.status(200);
            res.json(comment);
        } else {
            res.status(404);
            res.json({ message: 'Comment non trouvé' });
        }
    } catch (error) {
        res.status(500);
        console.log(error);
        res.json({ message: 'Erreur serveur' });
    }
}

exports.editAComment = async (req, res) => {
    const commentId = req.params.commentId;
    const updatedCommentData = req.body;
    try {
        const updatedComment = await Comment.findByIdAndUpdate(commentId, updatedCommentData, { new: true });
        if (updatedComment) {
            res.status(200);
            res.json(updatedComment);
        } else {
            res.status(404);
            res.json({ message: 'Comment non trouvé' });
        }
    } catch (error) {
        res.status(500);
        console.log(error);
        res.json({ message: 'Erreur serveur' });
    }
}

exports.deleteAComment = async (req, res) => {
    const commentId = req.params.commentId;
    try {
        const deletedComment = await Comment.deleteOne({"_id": commentId});
        if (deletedComment) {
            res.status(200);
            res.json({ message: 'Comment supprimé avec succès' });
        } else {
            res.status(404);
            res.json({ message: 'Comment non trouvé' });
        }
    } catch (error) {
        res.status(500);
        console.log(error);
        res.json({ message: 'Erreur serveur' });
    }
}