module.exports = (server) => {
      const postController = require('../controllers/postController');
      const jwtMiddleware = require("../middlewares/jwtMiddleware");
  
      /**
       * @swagger
       * tags:
       *   name: Posts
       *   description: Opérations liées aux posts
       */
  
      /**
       * @swagger
       * /posts:
       *   get:
       *     summary: Liste tous les posts
       *     tags: [Posts]
       *     responses:
       *       200:
       *         description: Succès de la récupération des posts
       *       500:
       *         description: Erreur serveur
       *   post:
       *     summary: Crée un nouveau post
       *     tags: [Posts]
       *     security:
       *       - Bearer: []
       *     parameters:
       *       - in: body
       *         name: body
       *         description: Détails d'un nouveau post
       *         required: true
       *         schema:
       *           type: object
       *           properties:
       *             title:
       *               type: string
       *             content:
       *               type: string
       *           required:
       *             - title
       *             - content
       *     responses:
       *       201:
       *         description: Post créé avec succès
       *       401:
       *         description: Non autorisé
       *       403:
       *         description: Action interdite
       *       500:
       *         description: Erreur serveur
       */
  
      server.route('/posts')
            .get(postController.listAllPosts)
            .post(jwtMiddleware.verifyToken, jwtMiddleware.isAdmin, postController.createAPost);
  
      /**
       * @swagger
       * /posts/{postId}:
       *   parameters:
       *     - name: postId
       *       in: path
       *       description: ID du post
       *       required: true
       *       type: string
       *   get:
       *     summary: Obtient les détails d'un post
       *     tags: [Posts]
       *     security:
       *       - Bearer: []
       *     responses:
       *       200:
       *         description: Succès de la récupération des détails du post
       *       401:
       *         description: Non autorisé
       *       500:
       *         description: Erreur serveur
       *   put:
       *     summary: Modifie les détails d'un post
       *     tags: [Posts]
       *     security:
       *       - Bearer: []
       *     parameters:
       *       - in: body
       *         name: body
       *         description: Détails du post à modifier
       *         required: true
       *         schema:
       *           type: object
       *           properties:
       *             title:
       *               type: string
       *             content:
       *               type: string
       *           required:
       *             - title
       *             - content
       *     responses:
       *       200:
       *         description: Succès de la modification du post
       *       401:
       *         description: Non autorisé
       *       403:
       *         description: Action interdite
       *       500:
       *         description: Erreur serveur
       *   delete:
       *     summary: Supprime un post
       *     tags: [Posts]
       *     security:
       *       - Bearer: []
       *     responses:
       *       204:
       *         description: Post supprimé avec succès
       *       401:
       *         description: Non autorisé
       *       403:
       *         description: Action interdite
       *       500:
       *         description: Erreur serveur
       */
  
      server.route('/posts/:postId')
            .all(jwtMiddleware.verifyToken)
            .get(postController.getAPost)
            .put(jwtMiddleware.isAdmin, postController.editAPost)
            .delete(jwtMiddleware.isAdmin, postController.deleteAPost);
  }