module.exports = (server) => {
      const commentController = require('../controllers/commentController');
      const jwtMiddleware = require("../middlewares/jwtMiddleware");
  
      /**
       * @swagger
       * tags:
       *   name: Commentaires
       *   description: Opérations liées aux commentaires
       */
  
      /**
       * @swagger
       * /posts/{postId}/comments:
       *   parameters:
       *     - name: postId
       *       in: path
       *       description: ID du post
       *       required: true
       *       type: string
       *   get:
       *     summary: Liste tous les commentaires d'un post
       *     tags: [Commentaires]
       *     responses:
       *       200:
       *         description: Succès de la récupération des commentaires
       *       500:
       *         description: Erreur serveur
       *   post:
       *     summary: Crée un nouveau commentaire pour un post
       *     tags: [Commentaires]
       *     security:
       *       - Bearer: []
       *     parameters:
       *       - in: body
       *         name: body
       *         description: Commentaire
       *         required: true
       *         schema:
       *           type: object
       *           properties:
       *             name:
       *               type: string
       *             message:
       *               type: string
       *           required:
       *             - name
       *             - message
       *     responses:
       *       201:
       *         description: Commentaire créé avec succès
       *       401:
       *         description: Non autorisé
       *       500:
       *         description: Erreur serveur
       */
  
      server.route('/posts/:postId/comments')
            .get(commentController.listAllComments)
            .post(jwtMiddleware.verifyToken, commentController.createAComment);
  
      /**
       * @swagger
       * /comments/{commentId}:
       *   parameters:
       *     - name: commentId
       *       in: path
       *       description: ID du commentaire
       *       required: true
       *       type: string
       *   get:
       *     summary: Obtient les détails d'un commentaire
       *     tags: [Commentaires]
       *     responses:
       *       200:
       *         description: Succès de la récupération des détails du commentaire
       *       401:
       *         description: Non autorisé
       *       500:
       *         description: Erreur serveur
       *   put:
       *     summary: Modifie le contenu d'un commentaire
       *     tags: [Commentaires]
       *     security:
       *       - Bearer: []
       *     parameters:
       *       - in: body
       *         name: body
       *         description: Détails du commentaire à modifier
       *         required: true
       *         schema:
       *           type: object
       *           properties:
       *             message:
       *               type: string
       *           required:
       *             - message
       *     responses:
       *       200:
       *         description: Succès de la modification du commentaire
       *       401:
       *         description: Non autorisé
       *       403:
       *         description: Action interdite
       *       500:
       *         description: Erreur serveur
       *   delete:
       *     summary: Supprime un commentaire
       *     tags: [Commentaires]
       *     security:
       *       - Bearer: []
       *     responses:
       *       204:
       *         description: Commentaire supprimé avec succès
       *       401:
       *         description: Non autorisé
       *       403:
       *         description: Action interdite
       *       500:
       *         description: Erreur serveur
       */
  
      server.route('/comments/:commentId')
            .get(commentController.getAComment)
            .put(jwtMiddleware.verifyToken, jwtMiddleware.isAdmin, commentController.editAComment)
            .delete(jwtMiddleware.verifyToken, jwtMiddleware.isAdmin, commentController.deleteAComment);
  }
  